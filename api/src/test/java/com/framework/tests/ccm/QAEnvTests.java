package com.framework.tests.ccm;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import org.json.JSONObject;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.frameowrk.utilities.APITestBase;
import com.frameowrk.utilities.TestUtil;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class QAEnvTests extends APITestBase {

	@BeforeMethod
	public void setup() {
		RestAssured.useRelaxedHTTPSValidation();
	}

	@Test(priority = 0, groups = { "Smoke" })
	public void validateGetResponse()
	{
		// Initialize Request specification with url
		initializeRequestSpecification();
		// validate Status Code 
		validateResponseCode(200);
		// Print Response
		validateResponseToHaveJsonData();
		//Get Response Body 
		getResponseBody();
		// Validate Json key Value Expected Data
		validateJsonKeyValuePair();
		// Validate Json Data
		validateJsonData();
			
	}

	@Test(priority = 1, groups = { "Smoke" })
	public void validatePostRuleJson() {
		// Initialize Request specification with url
		initializeRequestSpecification();
		// Print Response
		returnResponseWithQueryParameters();
//		// validate Status Code 
//		validateResponseCode(200);
//		// Print Response
//		validateResponseToHaveJsonData();
//		//Get Response Body 
//		getResponseBody();
//		// Validate Json key Value Expected Data
//		validateJsonKeyValuePair();
//		//System.out.println(test.returnTestCaseParameterRowCoordinate(1));
//		// Validate Json Data
//		validateJsonData();
		
		
//		RestAssured.baseURI ="https://ev-qa02.zs.local/CCM/0071uxpo/api/integration/executionRevision?buIds=2&labels=HUMAN";
//		 RequestSpecification request = RestAssured.given();
//		 
//		 JSONObject requestParams = new JSONObject();
//		 requestParams.put("Name", "abcd"); // Cast
//		 request.body(requestParams.toString());
//		 Response response = request.post("/register");
//		 System.out.println("Response is" + response.body().asString());
//		
	}
}
