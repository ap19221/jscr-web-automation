package com.frameowrk.utilities;

import static io.restassured.RestAssured.given;

import java.io.File;

import org.json.JSONObject;

import com.framework.environment.setup.EnvironmentVariables;

import io.restassured.RestAssured;
import io.restassured.authentication.AuthenticationScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIHelper extends EnvironmentVariables {
	
	public static RequestSpecBuilder requestBuilder;
	public static RequestSpecification requestSpec;
	public static Response resp;
	static EnvironmentVariables env = new EnvironmentVariables();
	
	private static String payload = "{ \"Name\": \"string\"}";
	
	 TestUtil utility = new TestUtil();
	
	// Request Specification Initializer
	public  void initializeRequestSpecification() {
		baseurl=env.setbaseurl();
		AuthenticationScheme authSchema = RestAssured.basic(env.getPropertyValue("username"), env.getPropertyValue("password"));
		requestBuilder = new RequestSpecBuilder();
			//Covert to Map take values from Excel	
		requestBuilder.addHeader("Authorization", env.getPropertyValue("authorization"));
		requestBuilder.addHeader("Content-Type", env.getPropertyValue("responsetype"));
		requestBuilder.addHeader("X-Actual-User", env.getPropertyValue("x-actualuser"));
		requestBuilder.addHeader("Host", env.getPropertyValue("host"));
		System.out.println("Json data as json array "+ utility.returnJsonData(4));
		System.out.println("Json data as string "+ utility.returnTestCaseParameterRowCoordinate(4));
		
		System.out.println("Body is" + utility.returnTestCaseParameterRowCoordinate(4).toString());
		System.out.println("baseurl :" + baseurl+  utility.returnTestCaseParameterRowCoordinate(1)+utility.returnTestCaseParameterRowCoordinate(2));
		//requestBuilder.setContentType(ContentType.JSON).setAccept(ContentType.JSON).setBody("[{\"Name\": \"string\"}]");
		requestBuilder.setBaseUri(baseurl+  utility.returnTestCaseParameterRowCoordinate(1)+utility.returnTestCaseParameterRowCoordinate(2));
		requestBuilder.setAuth(authSchema);
		requestSpec = requestBuilder.build();
		//requestSpec.contentType(ContentType.JSON).body("{\"Name\": \"string\"}");
	}
	
	// Return Get API Response
	public Response returnResponseWithQueryParameters() {
		String payload = "";
	    
		if(utility.returnTestCaseParameterRowCoordinate(3).toString().equalsIgnoreCase("get"))
		{
		resp = given().spec(requestSpec).when().get().then().assertThat().statusCode(200).extract()
					.response();
		}
		else if(utility.returnTestCaseParameterRowCoordinate(3).toString().equalsIgnoreCase("post"))
		{
		//System.out.println("Specification:" + requestSpec.bget().getBody());
		System.out.println("Post body is"+utility.returnJsonData(4));
		//requestSpec.contentType(ContentType.JSON).body("{\"Name\": \"string\"}");
		JSONObject requestParams = new JSONObject();
		requestParams.put("Name", "abcd");
		
		resp = RestAssured.given().spec(requestSpec).body(requestParams.toString()).when().post().then().assertThat().statusCode(200).extract().response();	
		System.out.println("Response :" + resp);
		}
		else if(utility.returnTestCaseParameterRowCoordinate(3).toString().equalsIgnoreCase("get"))
		{
		//System.out.println("Specification:" + requestSpec.get().getBody());
		resp = given().spec(requestSpec).contentType(ContentType.JSON).body(utility.returnTestCaseParameterRowCoordinate(4)).put().then().assertThat().statusCode(200).extract().response();	
		}
		else
		{
		resp = given().spec(requestSpec).when().delete().then().assertThat().statusCode(200).extract().response();
		}
		return resp;

	}
	
	
	
}
