package com.frameowrk.utilities;

import org.apache.commons.logging.Log;
import org.json.JSONArray;
import org.testng.Assert;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

//import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.ValidatableResponse;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema; 

public class APITestBase extends APIHelper  {

	
	TestUtil utility = new TestUtil();
	
	// Validate Status Code
	public void validateResponseCode(int statusCode) {
		// Get Response Data
		resp = returnResponseWithQueryParameters();	
		//Get Status Code & Validate
		ValidatableResponse responseStatuscode = resp.then().assertThat().statusCode(statusCode);
		//log.info("Expected Status Code :" + statusCode + "/n Status Code Recieved : " + responseStatuscode);	
		System.out.println("Expected Status Code :" + statusCode + "/n Status Code Recieved : " + responseStatuscode);
		}

	// Validate Response Data to have Json Data
	public void validateResponseToHaveJsonData() {
		System.out.println("Response Type :" + returnResponseWithQueryParameters().getHeaders().getValues("Content-Type").toString().contains(utility.returnTestCaseParameterRowCoordinate(5).toString()));
		try {
			Assert.assertEquals(returnResponseWithQueryParameters().getHeaders().getValues("Content-Type").toString().contains(utility.returnTestCaseParameterRowCoordinate(5).toString()), true);
			System.out.println("Headers Validated with Content-type Value is : "
					+ returnResponseWithQueryParameters().getHeaders().getValues("Content-Type").toString());
			//Log.info("Response Header is : " + returnResponseWithQueryParameters().getHeaders().getValues("Content-Type").toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// Get Response Body 
	public String getResponseBody()
	{
		System.out.println("Response Body is : "+ returnResponseWithQueryParameters().getBody().asString());
		//log.info("Response Body is : "+ returnResponseWithQueryParameters(apiType,"").getBody().asString());
		return returnResponseWithQueryParameters().getBody().asString();
	}
	
	// Validate Json Key Value
	public void validateJsonKeyValuePair() {
		try {
			// Validate Json Key value 
			System.out.println(utility.returnTestCaseParameterRowCoordinate(8).toString());
			Assert.assertEquals(getResponseBody().contains(utility.returnTestCaseParameterRowCoordinate(8).toString()), true );
			System.out.println("Expeceted Data matches with the Response");
			//log.info("API Type Contains Expeceted Data");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Expeceted Data doesn't match with the Response");
			//log.info("API Type does not matches with the Expeceted Data");
		}
		
	}

	// Validate Json Complete Structure
	public void validateJsonData()
	{
		//JsonSchemaValidator validator = JsonSchemaValidator.matchesJsonSchemaInClasspath("C:\\ZS_Projects\\API branch\\ccm-automation\\src\\test\\resources\\ExpectedFiles\\QA\\Question_ExpectedData.json");
		//Hamcrest Matcher to Check
		System.out.println("Response is :" + returnResponseWithQueryParameters());
		System.out.println("Expected Response is :" + utility.returnJsonData(9));
		
		returnResponseWithQueryParameters().getBody().asString();
		JSONArray jsonResponseArray =  new JSONArray(returnResponseWithQueryParameters().getBody().asString());
		JSONArray jsonExpectedJsonData = utility.returnJsonData(9);
		System.out.println("Json Response Data" + jsonExpectedJsonData);
		JsonParser parser = new JsonParser();
		Assert.assertEquals(parser.parse(returnResponseWithQueryParameters().getBody().asString()), parser.parse(utility.returnJsonData(9).toString()));
		
	}
	
 
	
	
}
