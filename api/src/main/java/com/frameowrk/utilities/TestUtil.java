package com.frameowrk.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.testng.ITestResult;

import com.framework.environment.setup.EnvironmentVariables;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TestUtil extends EnvironmentVariables{

	EnvironmentVariables env = new EnvironmentVariables();
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	FileInputStream ExcelFile;
	XSSFRow Row = null; 
	XSSFRow row = null;
	ITestResult test;
	String path = env.expectedTestDataLocation;
	String SheetName = "RequestParameters";

 
 public XSSFCell returnTestCaseParameterRowCoordinate(int columnNumber)
 {
	 int rowNumber = 0;
	 try {
		 ExcelFile = new FileInputStream(path);
		 // Access the required test data sheet
		ExcelWBook = new XSSFWorkbook(ExcelFile);
		ExcelWSheet = ExcelWBook.getSheet(SheetName);
		Row = ExcelWSheet.getRow(0);
		int rowCount = ExcelWSheet.getLastRowNum() + 1;
		
		for(rowNumber = 0 ; rowNumber <= rowCount ;rowNumber ++) {
			ExcelWSheet.getRow(rowNumber).getCell(1).equals("validatePostRuleJson");
			break;
		}
					
		ExcelFile.close();
   	
			
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 return ExcelWSheet.getRow(rowNumber+1).getCell(columnNumber);
		
 }

 public JSONArray returnJsonData(int columnNumber)
 {
	 	JSONArray  JsonArray = null;
		try {
			ExcelFile = new FileInputStream(path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			try {
				JsonArray =  new JSONArray(returnTestCaseParameterRowCoordinate(columnNumber).toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("JsonArray is :" + JsonArray);
		
			//System.out.println("Json Object is" + jsonObject);
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return JsonArray;
		}
 
}
