package com.framework.environment.setup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class EnvironmentVariables {

	public String enviornment = System.getenv("enviornment");
	public static String baseurl = System.getenv("baseurl");
	public String testDataLocation = "src/test/resources/TestData/" + setEnviornment() + "/TestData.xlsx";
	public String envProperties = "src/test/resources/EnviornmentConfigs/" + setEnviornment() + "/env.properties";
	public static String defaultEnvProperties = "src/test/resources/EnviornmentConfigs/QA/env.properties";
	public static String defaulttestDataLocation = "src/test/resources/TestData/QA/TestData.xlsx";
	public String expectedFileBasePath = "src/test/resources/ExpectedFiles/" + enviornment;
	public String expectedTestDataLocation = "src/test/resources/TestData/" + enviornment + "/TestData.xlsx";
	public static Properties prop = new Properties();

	// Set Enviornment Name
	public String setEnviornment() {
		System.out.println("Initial Env:" + enviornment);
		if (System.getenv("enviornment") == null) {
			try {
				prop.load(new FileInputStream(defaultEnvProperties));
				enviornment = prop.getProperty("enviornment");
				// System.out.println(enviornment);
				envProperties = "src/test/resources/EnviornmentConfigs/" + enviornment + "/env.properties";
				System.out.println("envproperties" + envProperties);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// System.out.println("env" + getPropertyValue("enviornment"));
		} else {
			enviornment = System.getenv("enviornment");
			envProperties = "src/test/resources/EnviornmentConfigs/" + enviornment + "/env.properties";
		}
		return enviornment;
	}
	
	/// Set Base Url
	public String setbaseurl() {
		System.out.println(baseurl);
		if (System.getenv("baseurl") == null) {
			try {
				prop.load(new FileInputStream(defaulttestDataLocation));
				enviornment = prop.getProperty("enviornment");
				baseurl = prop.getProperty("baseurl");
				System.out.println("baseurl in env.properties " + baseurl);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("env" + getPropertyValue("enviornment"));

		} else {
			baseurl = System.getenv("baseurl");
		}
		return baseurl;
	}

	// Get Property File Values
	public String getPropertyValue(String propertyName) {
		try {
			prop = new Properties();
			FileInputStream input = new FileInputStream(envProperties);
			System.out.println("Sheet : " + expectedTestDataLocation);
			// load a properties file
			prop.load(input);
			// get the property value and print it out
			System.out.println(prop.getProperty(propertyName));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return prop.getProperty(propertyName);
	}

	// Return File Value
	public File getExpectedFile(String ExpectedFileName)
	{
		File file = new File(expectedFileBasePath+"/"+ExpectedFileName);
		System.out.println("File Passed" + file);
		return file;
	}
		
	// Get Property File Values
	public BufferedReader getPropertyExpectedFileLocation(String propertyName,String ExpectedFileName) {
		
		
		BufferedReader br = null;
		try {
			File file = new File(expectedFileBasePath+"/"+ExpectedFileName);
			br = new BufferedReader(new FileReader(file)); 
			  
			System.out.println("Expected File Location:" + expectedFileBasePath+"/"+ExpectedFileName);
						
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return br;
		
	}
	
	
}
