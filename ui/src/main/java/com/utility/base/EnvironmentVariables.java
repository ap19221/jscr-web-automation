package com.utility.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.utility.customlisteners.WebEventListener;

public class EnvironmentVariables extends TestBase{

	static String browser = System.getenv("browser");
	static String JIM_URL = System.getenv("JIM_URL");
	

	public static String browser() {

		if (browser == null) {
			browser = prop.getProperty("browser");
		}
		return browser;
	}
	

	public static String JIM_URL() {

		if (JIM_URL == null) {
			JIM_URL = prop.getProperty("url");
		}
		return JIM_URL;
	}

	}
