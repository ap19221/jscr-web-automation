package com.utility.base;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utility.customlisteners.WebEventListener;
import com.utility.logger.Log;

public class WrapperFunctionUtilities extends TestBase {
	public static WebDriver dr;
	public static EventFiringWebDriver driver;
	public WebEventListener eventListener;
	public static String TESTDATA_SHEET_PATH = PROJECT_PATH + "/src/test/java/com/projectName/testdata/TestData.xlsx";

	public static void waitForElementVisibility(WebElement element, int time, WebDriver driver) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.visibilityOf(element));
			Log.info("Element is visible");
		} catch (ElementNotVisibleException e) {
			Log.info("Element is not visible");
		}

		catch (Exception e) {
			Log.info("Element is not visible");
		}
	}

	public static void waitforElementToLoad(long timeoutseconds, WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, timeoutseconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void waitForPageToLoad(int time, WebDriver driver) {

		driver.manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
		Log.info("Page is loaded successfully");
	}

	public static void waitForTime(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void getURL(WebDriver driver, String url) {
		Log.info("Navigating to the URL:" + url);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	// Method retrieves all the windows open for a driver in a iterator
	public static Iterator<String> getAllWindowssID(WebDriver driver) {
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> itr = windows.iterator();
		return itr;
	};

	public static void closeBrowser(WebDriver driver) {
		driver.close();
		Log.info("Browser Closed");
	}

	public static void driverQuit(WebDriver driver) {
		driver.quit();
		Log.info("Browser Thread Closed");

	}

	public static void findElementByVisibleText(WebElement element, String Text, WebDriver driver) {
		Select findText = new Select(element);
		findText.selectByVisibleText(Text);

	}

	public static boolean isTextPresent(String text, WebDriver driver) {
		Log.info("Checking the presence of Text");
		@SuppressWarnings("unused")
		Boolean element = null;
		try {
			driver.getPageSource().contains(text);
			Log.info("Text is present...");

			return true;
		} catch (Exception e) {
			Log.info("Text is not present...");
		}
		return false;
	}

	public static boolean isElementPresent(WebElement ele, String name) {
		WrapperFunctionUtilities.waitForTime(5000);
		if (ele.isDisplayed()) {
			Log.info(name + " is displayed");
			return true;
		} else {
			Log.info(name + " is Not displayed");
			return false;
		}

	}

	public static boolean isElementVisible(int maxwaittime, WebElement ele, WebDriver driver) {
		Log.info("Checking the visiblity of Element");
		@SuppressWarnings("unused")
		WebElement element = null;
		try {
			// driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, maxwaittime);
			element = wait.until(ExpectedConditions.visibilityOf(ele));
			Log.info("Element is visible...");

			return true;
		} catch (org.openqa.selenium.TimeoutException e) {
			Log.info("Element not visible...");
		}
		return false;
	}

	// Switching to the frame
	public static void switchFrame(WebDriver driver, String frame) {
		try {
			driver.switchTo().frame(frame);
		} catch (NoSuchFrameException e) {
			Log.info("No frame exception : ");
		}

		catch (StaleElementReferenceException e) {
			Log.info("Stale element exception : ");
		}

		catch (NoSuchElementException e) {
			Log.info("No Such Element exception : ");
		}

		catch (ElementNotVisibleException e) {
			Log.info("Element Not Visible exception : ");
		}

		catch (Exception e) {
			Log.info("Element Not Visible");
		}

	}

	public static boolean isElementVisibleForTime(WebElement ele, int time, WebDriver driver) {
		Log.info("Checking the visibility of Element ");
		@SuppressWarnings("unused")
		WebElement element = null;
		try {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, time);
			element = wait.until(ExpectedConditions.visibilityOf(ele));
			Log.info("Element is visible...");
			return true;
		} catch (org.openqa.selenium.TimeoutException e) {
			Log.info("Element not visible...");
		}
		return false;
	}

	public void refreshPage(WebDriver driver) {
		driver.navigate().refresh();
	}

	public static void MouseOver(WebElement we, WebDriver driver) throws Exception {
		Actions actObj = new Actions(driver);
		actObj.moveToElement(we).build().perform();

	}

	public static void selectObjectByIndexValue(WebElement we, int indexvalue) throws Exception {
		Select selObj = new Select(we);
		selObj.selectByIndex(indexvalue);
	}

	public static void selectObjectByStringValue(WebElement we, String value) throws Exception {
		Select selObj = new Select(we);
		selObj.selectByValue(value);
	}

	// Scroll to the element
	public static void scrollByVisibleElement(WebElement we, WebDriver driver) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", we);
	}

	// Scroll till the end of the page
	public static void scrollDown(WebDriver driver) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	// Scroll Up in Vertical Direction
	public static void scrollUp(WebDriver driver) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(document.body.scrollHeight,0)");
	}

	// Click By JavaScipt
	public static void jsClick(WebElement ele, WebDriver driver) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", ele);
	}

	// Get the system current date and time
	public static String getCurrentDateTime() throws Exception {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		return date;
	}

	// Accepting the Popup
	public static void acceptAlert(WebDriver driver) throws Exception {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	// Rejecting the Popup
	public static void dismissAlert(WebDriver driver) throws Exception {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public static String readUsername() {
		XSSFWorkbook workbook = null;
		try {
			FileInputStream fis = new FileInputStream(TESTDATA_SHEET_PATH);
			workbook = new XSSFWorkbook(fis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		XSSFSheet sheet = workbook.getSheetAt(0);
		Row row = sheet.getRow(1);
		Cell cell = row.getCell(0);
		String cellVal = cell.getStringCellValue();
		return cellVal;

	}

	public static String readPassword() {
		XSSFWorkbook workbook = null;
		try {
			FileInputStream fis = new FileInputStream(TESTDATA_SHEET_PATH);
			workbook = new XSSFWorkbook(fis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		XSSFSheet sheet = workbook.getSheetAt(0);
		Row row = sheet.getRow(1);
		Cell cell = row.getCell(1);
		String cellVal = cell.getStringCellValue();
		return cellVal;
	}

	// Add & edit Text Area
	public static void addEditTextarea(String query, WebElement textarea) {
		// this works for CodeMirror
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].CodeMirror.setValue( '" + query + "');", textarea);
	}

	// Upload File Function
	public static void uploadFile(WebElement ele, String fileName, String extension) {
		String path = PROJECT_PATH + "/src/test/java/com/projectName/testdata/" + fileName + "." + extension;
		ele.sendKeys(path);
	}

	// Wait till Element is Present
	public static void waitTillElementIsPresent(WebElement ele, int timeInterval) {
		boolean x = ele.isDisplayed();
		while (x == false) {
			WrapperFunctionUtilities.waitForTime(timeInterval);
			x = ele.isDisplayed();
		}
		return;
	}

	// Wait till Invisibility of Element
	public static void waitForElementInVisibility(List<WebElement> element, int time, WebDriver driver) {
		try {
			if (element.size() > 0) {
				WebDriverWait wait = new WebDriverWait(driver, time);
				wait.until(ExpectedConditions.invisibilityOf(element.get(0)));
				Log.info("Element is not visible now");
			}
		} catch (ElementNotInteractableException e) {
			Log.info("Element is still visible");
		} catch (Exception e) {
			Log.info("Element is still visible");
		}
	}

	// Reload Page
	public static void refreshomePageObject() {
		driver.navigate().refresh();
	}

	// Override Implicit Wait
	public static void overrideImplicitWait(int timeinseconds) {
		driver.manage().timeouts().implicitlyWait(timeinseconds, TimeUnit.SECONDS);
	}

	// Clear Existing Text & Send Value in Text Area
	public static void clearAndSend(WebElement element, Object value) {
		element.clear();
		element.sendKeys(value.toString());
	}

	// Select the value by Visible Text Area
	public static void selectWithValue(WebElement element, String text) {
		Select s = new Select(element);
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO: handle exception
		}
		s.selectByVisibleText(text);
	}

	// Select a Particular in List
	public static boolean searchItemInList(List<WebElement> list, String text) {
		boolean itemstatus = false;
		for (WebElement element : list) {
			if (element.getAttribute("filter-name").equalsIgnoreCase(text)) {
				itemstatus = true;
			} else {
				itemstatus = false;
			}
		}
		return itemstatus;
	}

	// Download the file on Local
	public static boolean isFileDownloaded(String fileName) {
		String Source = System.getProperty("user.home") + "/Downloads/";
		boolean flag = false;
		File dir = new File(Source);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}

		return flag;
	}

	// Navigate to Another Enviornement Url
	public static void navigateToAnotherEnvironment(String url) {
		driver.navigate().to(url);
	}

	// Get Current Time Stamp
	public static String getCurrentDateTime(String pattern) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		return date;
	}

	public static void MouseOverForToolTip(WebElement we, WebDriver driver) {
		try {
			Actions actObj = new Actions(driver);
			actObj.moveToElement(we).click(we).build().perform();
			Thread.sleep(3000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("unable to hover");
		}
	}

	public static void scrollDownWithElement(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollTop = arguments[1];", element, 500);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
