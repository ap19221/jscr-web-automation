package com.projectName.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.utility.base.TestBase;
import com.utility.base.WrapperFunctionUtilities;
import com.utility.logger.Log;

public class HomePageTest extends TestBase {
	

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) {
		Log.info("Browser passed as :- " + browser);
		if(browser.equals(""))
		{
			browser = prop.getProperty("browser");
		}
		initialization(browser);
		
	}

	// write your test in here. you can refer below sample test (commented) as
	// reference .
	// This method verifies the homepage is loaded and clicks on logout

	/*
	 * 
	 * @Test(priority = 1) public void verifyHomePageTitleTest() {
	 * IDMLoginPageFunctions.signIn(prop.getProperty("username"),
	 * prop.getProperty("password")); String title =
	 * HomePageFunctions.verifyHomePageTitle();
	 * Assert.assertTrue(title.contains("Javelin")); String afterLogout =
	 * HomePageFunctions.clickOnlogoutButton(); Assert.assertEquals(afterLogout,
	 * "Javelin - Sign In");
	 * 
	 * }
	 * 
	 */

	@AfterMethod
	public void tearDown() {
		WrapperFunctionUtilities.closeBrowser(driver);
	}

}
