package com.projectName.testcases;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.projectName.pagefunctions.LoginPageFunctions;
import com.idm.pagefunctions.IDMLoginPageFunctions;
import com.utility.base.TestBase;
import com.utility.base.WrapperFunctionUtilities;
import com.utility.logger.Log;

public class LoginPageTest extends LoginPageFunctions {

	LoginPageFunctions loginPage;

	// initializing the drivers
	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) {
		Log.info("Browser passed as :- " + browser);
		if(browser.equals(""))
		{
			browser = prop.getProperty("browser");
		}
		initialization(browser);
		
	}

	// write your test in here. you can refer below sample test (commented) as
	// reference .
	// Test to verify successful Login

	/*
	 * @Test(priority = 2) public void loginTest() {
	 * IDMLoginPageFunctions.signIn(prop.getProperty("username"),
	 * prop.getProperty("password"));
	 * Assert.assertEquals(IDMLoginPageFunctions.validateHomepageUsername(),
	 * prop.getProperty("username"));
	 * Assert.assertEquals(IDMLoginPageFunctions.validateMyAccountdropdown(), true);
	 * Log.info("Successfully Logged in with valid user "); }
	 * 
	 */

	// Closing of the browser
	@AfterMethod
	public void tearDown() {
		WrapperFunctionUtilities.closeBrowser(driver);

	}

}
