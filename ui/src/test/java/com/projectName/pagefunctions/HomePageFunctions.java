// You can add your applications Home page specific  functions here 

package com.projectName.pagefunctions;

import org.openqa.selenium.support.PageFactory;

import com.projectName.locators.HomePage;

public class HomePageFunctions extends HomePage {

	// Initializing the Page Objects:
	public HomePageFunctions() {
		PageFactory.initElements(driver, this);
	}

	/*
	 * verifyHomePageTitle gets the title for Homepage
	 */

	public static String verifyHomePageTitle() {

		return driver.getTitle();
	}

	/*
	 * clicking on logout button
	 */
	public static String clickOnlogoutButton() {
		String title = driver.getTitle();
		//logout.click();
		return driver.getTitle();

	}

}
