package com.projectName.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utility.base.TestBase;

public class HomePage extends TestBase {

	/*
	 * This class provides locators for Homepage
	 */
	
	@FindBy(id = "ctl00_FormSubTitle")
	WebElement homepageVerification;

	@FindBy(xpath = "//a[@id='lbl_so']")
	public static WebElement logout;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

}
