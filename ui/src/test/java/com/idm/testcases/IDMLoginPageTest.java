package com.idm.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.idm.pagefunctions.IDMLoginPageFunctions;
import com.utility.base.TestBase;
import com.utility.base.WrapperFunctionUtilities;
import com.utility.logger.Log;

public class IDMLoginPageTest extends TestBase {

	IDMLoginPageFunctions loginPage;

	// Loading of Browser
	@BeforeMethod
	@Parameters("browser")
	public void setUp(@Optional String browser) {
		if (browser == null) {
			browser = prop.getProperty("browser");
		}
		Log.info("Browser passed as :- " + browser);
		initialization(browser);
		loginPage = new IDMLoginPageFunctions();
	}

	// This test verifies title of login page
	@Test(priority = 1)
	public void loginPageTitleTest() {
		String title = IDMLoginPageFunctions.validateLoginPageTitle();
		Assert.assertEquals(title, "Javelin - Sign In");
		Log.info("Website has been found");
	}

	// Test to verify successful Login
	@Test(priority = 2)
	public void loginTest() {
		Log.info("into loging page");
		IDMLoginPageFunctions.signIn(WrapperFunctionUtilities.readUsername(), WrapperFunctionUtilities.readPassword());
		Assert.assertEquals(IDMLoginPageFunctions.validateHomepageUsername(), WrapperFunctionUtilities.readUsername());
		//Assert.assertEquals(IDMLoginPageFunctions.validateMyAccountdropdown(), true);
		Log.info("Successfully Logged in with valid user ");
	}

	// Closing of the browser
	@AfterMethod
	public void tearDown() {
		WrapperFunctionUtilities.closeBrowser(driver);
		WrapperFunctionUtilities.driverQuit(driver);

	}

}
