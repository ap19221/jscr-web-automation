package com.idm.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utility.base.TestBase;

public class IDMLoginLandingPage extends TestBase {

	/*
	 * This class provides locators for Homepage
	 */

	@FindBy(id = "ctl00_FormSubTitle")
	public static WebElement homepageVerification;

	@FindBy(xpath = "//a[@id='lbl_so']")
	public static WebElement logout;

	public static WebElement profile = driver.findElement(By.xpath("//div[contains(@role,'profile')]"));

	public static WebElement myAcc = driver.findElement(By.id("lbl_ma"));;

	public IDMLoginLandingPage() {
		PageFactory.initElements(driver, this);
	}

}
