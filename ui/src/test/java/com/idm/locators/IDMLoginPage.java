package com.idm.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utility.base.TestBase;

public class IDMLoginPage extends TestBase {

	// Page Factory -

	/*
	 * This class provides locators for Login Page
	 * 
	 */

	
	
	@FindBy(id = "login_form")
	protected static WebElement loginform;

	@FindBy(name = "ctl00$PageContentPlaceholder$UserNameInput")
	protected static WebElement username;

	@FindBy(name = "ctl00$PageContentPlaceholder$PasswordInput")
	protected static WebElement pasword;

	@FindBy(name = "ctl00$PageContentPlaceholder$LoginButton")
	public static WebElement loginBtn;

	public IDMLoginPage() {
		PageFactory.initElements(driver, this);
	}

}
