package com.idm.pagefunctions;

import com.idm.locators.IDMLoginLandingPage;
import com.idm.locators.IDMLoginPage;
import com.utility.base.WrapperFunctionUtilities;
import com.utility.logger.Log;

public class IDMLoginPageFunctions extends IDMLoginPage {

	// Actions:
	/*
	 * Inserting of username and password
	 */



	public static void signIn(String userName, String password) {
		
		username.sendKeys(userName);
		Log.info("Username Entered");
		WrapperFunctionUtilities.waitForTime(1000);
		pasword.sendKeys(password);
		Log.info("Password Entered");
		loginBtn.click();

	}

	// Login Page Title Page Validation

	public static String validateLoginPageTitle() {
		Log.info(driver.getTitle());
		WrapperFunctionUtilities.waitForTime(1000);
		return driver.getTitle();

	}

	/*
	 * The below methods returns the username found on the Homepage of Javelin
	 * Incentive Manager
	 */

	public static String validateHomepageUsername() {
		WrapperFunctionUtilities.waitForTime(1000);
		String actualtext = IDMLoginLandingPage.profile.getText();
		return actualtext;
	}

	public static boolean validateMyAccountdropdown() {

		WrapperFunctionUtilities.waitForTime(5000);
		IDMLoginLandingPage.profile.click();
		WrapperFunctionUtilities.waitForTime(5000);
		return WrapperFunctionUtilities.isElementPresent(IDMLoginLandingPage.myAcc, "My Account");

	}

}
